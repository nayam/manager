package jp.gr.java_conf.ny.function

enum class CivicCenter(val string: String) {
    ASAHIGAOKA("旭ヶ丘"), KUROMATSU("黒松市民センター"), YAGIYAMA("八木山市民センター")
}