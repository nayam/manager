package jp.gr.java_conf.ny.function

import com.sun.javafx.PlatformUtil
import javafx.collections.FXCollections
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.*
import javafx.scene.control.cell.ComboBoxListCell
import org.openqa.selenium.By
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.remote.RemoteWebDriver
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait
import java.net.URL
import java.util.*


class Controller : Initializable {

    @FXML private lateinit var operateButton: Button

    @FXML private lateinit var operationListView: ListView<Operation>

    @FXML private lateinit var operatorField: TextField
    @FXML private lateinit var operand1Field: TextField
    @FXML private lateinit var operand2Field: TextField
    @FXML private lateinit var addOperationButton: Button

    private val operations by lazy { FXCollections.observableArrayList<Operation>() }

    private val driver by lazy { FirefoxDriver() }


    override fun initialize(location: URL?, resources: ResourceBundle?) {

        System.setProperty("webdriver.gecko.driver",
                when{
                    PlatformUtil.isWindows() ->
                        "C:/Users/Iruka/Downloads/geckodriver.exe"
                    PlatformUtil.isLinux()->
                        "/home/unit/Downloads/geckodriver"
                    else-> error("Unknown Platform")

                })
        Runtime.getRuntime().addShutdownHook(Thread({ driver.quit() }))


        operationListView.items = operations
        operationListView.isEditable = true
        operationListView.cellFactory = ComboBoxListCell.forListView(operations)

        val contextMenu = ContextMenu().apply {
            items.add(
                    MenuItem("削除").apply {
                        setOnAction {
                            val selected = operationListView.selectionModel.selectedItems
                            operations.removeAll(selected)
                        }
                    }
            )
        }

        operationListView.contextMenu = contextMenu

        addOperationButton.setOnAction {
            if (operatorField.text == "get") {
                operations.add(Get(operand1Field.text))
                return@setOnAction
            }
            operations.add(Op(operatorField.text, operand1Field.text, operand2Field.text))
        }

        operateButton.setOnAction { operations.forEach(driver::run) }
        /*
            {
                // driver.toC019RsvEmptyState(civicCenterField.text)
                WebDriverWait(driver, 20).until(ExpectedConditions.urlContains(C019RsvEmptyState.URL))

                val today = LocalDate.now()

                val availableDay = today.plusMonths(1).plusDays(10)
                val days = generateSequence(today) { it.plusDays(1) }
                        .takeWhile { availableDay.isAfter(it) }
                        .filter { it.dayOfWeek == DayOfWeek.SUNDAY || it.dayOfWeek == DayOfWeek.SATURDAY }
                        .toList()

                days.forEach {
                    println("$it の確認中")

                    val target = it

                    val ym = driver.getYearAndMonthOnCalendar()
                    val a = (target.year - ym.year) * 12 + (target.monthValue - ym.monthValue)
                    if (a > 0) {
                        for (i in 0..a) {
                            val nextMonth = WebDriverWait(driver, 10).until(
                                    ExpectedConditions.elementToBeClickable(
                                            By.cssSelector(
                                                    if (driver.getYearAndMonthOnCalendar().month == today.month)
                                                        C019RsvEmptyState.NEXT_MONTH_IF_THIS_MONTH_SELECTOR
                                                    else
                                                        C019RsvEmptyState.NEXT_MONTH_SELECTOR)))

                            nextMonth.click()
                            WebDriverWait(driver, 10).until(ExpectedConditions.stalenessOf(nextMonth))
                        }
                    } else if (a < 0) {
                        val nextMonth = WebDriverWait(driver, 10).until(
                                ExpectedConditions.elementToBeClickable(
                                        By.cssSelector(C019RsvEmptyState.PREVIOUS_MONTH_SELECTOR)))

                        nextMonth.click()
                        WebDriverWait(driver, 10).until(ExpectedConditions.stalenessOf(nextMonth))
                    }

                    assert(driver.getYearAndMonthOnCalendar().withDayOfMonth(1) == target.withDayOfMonth(1))

                    val dayButton = WebDriverWait(driver, 10)
                            .until(ExpectedConditions.elementToBeClickable(By.cssSelector(C019RsvEmptyState.toSelector(it))))
                    dayButton.click()
                    WebDriverWait(driver, 10).until(ExpectedConditions.stalenessOf(dayButton))

                    assert(driver.getTargetDayOnC019RsvEmptyState() == it)


                    val (morning, afternoon, night) = driver.getAvailabilitiesOnC019RsvEmptyState()
                    println("Morning: $morning")
                    println("Afternoon: $afternoon")
                    println("Night: $night")
                }
            }*/
    }
}


val timeOutInSeconds = 30L

fun RemoteWebDriver.run(operation: Operation) {
    when (operation) {
        is Op ->
            when (operation.operator) {
                "click" ->
                    WebDriverWait(this, timeOutInSeconds)
                            .until(ExpectedConditions.elementToBeClickable(By.cssSelector(operation.operand1)))
                            .click()
                "waitUntilUrlContains" ->
                    WebDriverWait(this, timeOutInSeconds).until(ExpectedConditions.urlContains(operation.operand1))
                "waitUntilPresenceOfElementLocated" ->
                    WebDriverWait(this, timeOutInSeconds)
                            .until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(operation.operand1)))
                "sendKeys" -> executeScript("arguments[0].value = arguments[1]", findElement(By.cssSelector(operation.operand1)), operation.operand2)
                else -> error("Unsupported Operation")
            }
        is Get -> get(operation.url)
    }
}