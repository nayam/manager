package jp.gr.java_conf.ny.function

import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Scene
import javafx.stage.Stage

class MyApplication2 : Application() {
    override fun start(stage: Stage) {
        stage.title = "Manager"

        stage.scene = Scene(FXMLLoader.load(javaClass.getResource("view.fxml")))
        stage.show()
    }


}
