package jp.gr.java_conf.ny.function

import jp.gr.java_conf.ny.index.C019RsvEmptyState
import jp.gr.java_conf.ny.index.HomeIndex
import jp.gr.java_conf.ny.index.RsvNameSearch
import jp.gr.java_conf.ny.index.RsvNameSearchResult
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.remote.RemoteWebDriver
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait
import java.time.LocalDate

object Functions {

    fun <T : RemoteWebDriver> T.getInfo(civicCenter: CivicCenter, days: Iterable<LocalDate>) {
        toC019RsvEmptyState(civicCenter.string)

        val today = LocalDate.now()
        println("今日は $today です")

        WebDriverWait(this, 20).until(ExpectedConditions.titleIs("仙台市市民利用施設予約システム"))

        days.forEach {
            println("$it の確認中")

            println("Year and Month on Calendar = ${getYearAndMonthOnCalendar()}")
            println("it = $it")

            println("カレンダー月めくり中")

            val target = it

            val ym = getYearAndMonthOnCalendar()
            val a = (target.year - ym.year) * 12 + (target.monthValue - ym.monthValue)
            if (a > 0) {
                for (i in 0..a) {
                    val nextMonth = WebDriverWait(this, 10).until(
                            ExpectedConditions.elementToBeClickable(
                                    By.cssSelector(
                                            if (getYearAndMonthOnCalendar().month == today.month)
                                                C019RsvEmptyState.NEXT_MONTH_IF_THIS_MONTH_SELECTOR
                                            else
                                                C019RsvEmptyState.NEXT_MONTH_SELECTOR)))

                    nextMonth.click()
                    WebDriverWait(this, 10).until(ExpectedConditions.stalenessOf(nextMonth))
                }
            } else if (a < 0) {
                val nextMonth = WebDriverWait(this, 10).until(
                        ExpectedConditions.elementToBeClickable(
                                By.cssSelector(C019RsvEmptyState.PREVIOUS_MONTH_SELECTOR)))

                nextMonth.click()
                WebDriverWait(this, 10).until(ExpectedConditions.stalenessOf(nextMonth))
            }

            assert(getYearAndMonthOnCalendar().withDayOfMonth(1) == target.withDayOfMonth(1))

            val dayButton = WebDriverWait(this, 10)
                    .until(ExpectedConditions.elementToBeClickable(By.cssSelector(C019RsvEmptyState.toSelector(it))))
            dayButton.click()
            WebDriverWait(this, 10).until(ExpectedConditions.stalenessOf(dayButton))

            assert(getTargetDayOnC019RsvEmptyState() == it)


            val (morning, afternoon, night) = getAvailabilitiesOnC019RsvEmptyState()
            println("Morning: $morning")
            println("Afternoon: $afternoon")
            println("Night: $night")
        }
    }

    fun <T : RemoteWebDriver> T.sendKeys(selector: String, keys: String) {
        executeScript("arguments[0].value = arguments[1]", findElement(By.cssSelector(selector)), keys)
    }

    fun <T : RemoteWebDriver> T.toC019RsvEmptyState(keyword: String) {
        toHomeIndex()
        WebDriverWait(this, 20).until(ExpectedConditions.urlContains(HomeIndex.URL))

        toRsvNameSearchFromHomeIndex()
        WebDriverWait(this, 20).until(ExpectedConditions.urlContains(RsvNameSearch.URL))

        toRsvNameSearchResultFromRsvNameSearch(keyword)
        WebDriverWait(this, 20).until(ExpectedConditions.urlContains(RsvNameSearchResult.URL))

        toC019RsvEmptyStateFromRsvNameSearchResult()
        WebDriverWait(this, 20).until(ExpectedConditions.urlContains(C019RsvEmptyState.URL))
    }

    fun <T : WebDriver> T.toHomeIndex() = get(HomeIndex.URL)

    fun <T : WebDriver> T.toRsvNameSearchFromHomeIndex() = WebDriverWait(this, 10)
            .until(ExpectedConditions.elementToBeClickable(By.cssSelector(HomeIndex.RSV_NAME_SEARCH_SELECTOR)))
            .click()

    fun <T : RemoteWebDriver> T.toRsvNameSearchResultFromRsvNameSearch(keyword: String) {
        WebDriverWait(this, 30)
                .until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(RsvNameSearch.TEXT_KEYWORD_SELECTOR)))
        //.sendKeys(keyword)

        sendKeys(RsvNameSearch.TEXT_KEYWORD_SELECTOR, keyword)

        WebDriverWait(this, 30)
                .until(ExpectedConditions.elementToBeClickable(By.cssSelector(RsvNameSearch.DO_SEARCH_SELECTOR)))
                .click()
    }

    fun <T : WebDriver> T.toC019RsvEmptyStateFromRsvNameSearchResult() = WebDriverWait(this, 10)
            .until(ExpectedConditions.elementToBeClickable(By.cssSelector(RsvNameSearchResult.DO_SELECT_SELECTOR)))
            .click()


    fun <T : WebDriver> T.getYearAndMonthOnCalendar(): LocalDate {
        val yearAndMonth = WebDriverWait(this, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(C019RsvEmptyState.YEAR_MONTH_ON_CALENDAR_SELECTOR)))
                .text

        val matcher = "(201[789])年(1|2|3|4|5|6|7|8|9|10|11|12)月".toPattern().matcher(yearAndMonth)

        if (!matcher.find()) {
            error("Matching failed")
        }

        val year = matcher.group(1).toInt()
        val month = matcher.group(2).toInt()

        return LocalDate.of(year, month, 1)
    }

    fun <T : WebDriver> T.getTargetDayOnC019RsvEmptyState(): LocalDate {
        val year = WebDriverWait(this, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(C019RsvEmptyState.YEAR_SELECTOR)))
                .text.toInt()

        val month = WebDriverWait(this, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(C019RsvEmptyState.MONTH_SELECTOR)))
                .text.toInt()

        val dayOfMonth = WebDriverWait(this, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(C019RsvEmptyState.DAY_SELECTOR)))
                .text.toInt()

        return LocalDate.of(year, month, dayOfMonth)
    }

    fun <T : WebDriver> T.getAvailabilitiesOnC019RsvEmptyState(): Triple<String, String, String> {
        val morning = WebDriverWait(this, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(C019RsvEmptyState.MORNING_SELECTOR)))
                .getAttribute("alt")
        val afternoon = WebDriverWait(this, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(C019RsvEmptyState.AFTERNOON_SELECTOR)))
                .getAttribute("alt")
        val night = WebDriverWait(this, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(C019RsvEmptyState.NIGHT_SELECTOR)))
                .getAttribute("alt")

        return Triple(morning, afternoon, night)
    }
}