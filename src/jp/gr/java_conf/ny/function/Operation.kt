package jp.gr.java_conf.ny.function

import org.openqa.selenium.By
import org.openqa.selenium.remote.RemoteWebDriver
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait
import java.io.Serializable

data class Op(
        val operator: String,
        val operand1: String,
        val operand2: String
) : Operation {
    override fun run(driver: RemoteWebDriver) {
        when (operator) {
            "get" -> driver.get(operand1)
            "click" ->
                WebDriverWait(driver, timeOutInSeconds)
                        .until(ExpectedConditions.elementToBeClickable(By.cssSelector(operand1)))
                        .click()
            "waitUntilUrlContains" -> WebDriverWait(driver, timeOutInSeconds).until(ExpectedConditions.urlContains(operand1))
            "waitUntilPresenceOfElementLocated" ->
                WebDriverWait(driver, timeOutInSeconds)
                        .until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(operand1)))
            "sendKeys" -> driver.executeScript("arguments[0].value = arguments[1]", driver.findElement(By.cssSelector(operand1)), operand2)
            else -> error("Unsupported Operation")
        }
    }

}

data class Get(val url: String) : Operation {
    override fun run(driver: RemoteWebDriver) {
        driver.get(url)
    }
}

interface Operation : Serializable {
    fun run(driver: RemoteWebDriver)
}