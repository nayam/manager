import java.time.ZonedDateTime

/**
 * Created by unit on 17/04/19.
 */
data class Event(
        val fromDateTime: ZonedDateTime,
        val toDateTime: ZonedDateTime,
        val location: Location,
        val tag: Tag
) {
    constructor(
            fromDateTime: String,
            toDateTime: String,
            location: Location,
            tag: Tag) : this(
            fromDateTime.let(ZonedDateTime::parse),
            toDateTime.let(ZonedDateTime::parse),
            location,
            tag)

}
