package jp.gr.java_conf.ny

import javafx.beans.value.ChangeListener
import javafx.beans.value.ObservableValue
import javafx.concurrent.Worker
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Button
import javafx.scene.control.TextField
import javafx.scene.web.WebEngine
import javafx.scene.web.WebView
import jp.gr.java_conf.ny.index.C019RsvEmptyState
import jp.gr.java_conf.ny.index.HomeIndex
import jp.gr.java_conf.ny.index.RsvNameSearch
import jp.gr.java_conf.ny.index.RsvNameSearchResult
import org.openqa.selenium.firefox.FirefoxDriver
import org.w3c.dom.html.HTMLElement
import java.net.URL
import java.util.*

class View : Initializable {

    @FXML private lateinit var loadButton: Button

    @FXML private lateinit var urlField: TextField

    @FXML private lateinit var webView: WebView

    @FXML private lateinit var selectorField: TextField

    @FXML private lateinit var clickButton: Button

    @FXML private lateinit var emptyConfirmationButton: Button

    @FXML private lateinit var scriptField: TextField

    @FXML private lateinit var scriptButton: Button


    override fun initialize(location: URL?, resources: ResourceBundle?) {
        webView.engine.loadWorker.stateProperty().addListener { _, oldValue, newValue -> println("$oldValue->$newValue") }

        loadButton.setOnAction { webView.engine.loadAndThen(urlField.text) { _, state -> println("aaa") } }
        clickButton.setOnAction { webView.engine.clickElementBySelectorAndThen(selectorField.text) { _, state -> println(state) } }

        emptyConfirmationButton.setOnAction {
            webView.engine.loadAndThen(HomeIndex.URL) {
                _, state ->
                webView.engine.clickElementBySelectorAndThen(HomeIndex.RSV_NAME_SEARCH_SELECTOR) {
                    _, state ->
                    webView.engine.inputTextToElementBySelectorAndThen(RsvNameSearch.TEXT_KEYWORD_SELECTOR, "旭ヶ丘市民センター")
                    webView.engine.clickElementBySelectorAndThen(RsvNameSearch.DO_SEARCH_SELECTOR) {
                        _, state ->
                        webView.engine.clickElementBySelectorAndThen(RsvNameSearchResult.DO_SELECT_SELECTOR) {
                            _, state ->
                            webView.engine.loadWorker.stateProperty().addListener(object : ChangeListener<Worker.State> {
                                override fun changed(observable: ObservableValue<out Worker.State>, oldValue: Worker.State, newValue: Worker.State) {
                                    when (newValue) {
                                        Worker.State.RUNNING -> Unit
                                        Worker.State.READY -> Unit
                                        Worker.State.SCHEDULED -> Unit
                                        Worker.State.CANCELLED -> webView.engine.loadWorker.stateProperty().removeListener(this)
                                        Worker.State.FAILED -> webView.engine.loadWorker.stateProperty().removeListener(this)
                                        Worker.State.SUCCEEDED -> {
                                            println(webView.engine.getElementBySelector(C019RsvEmptyState.AFTERNOON_SELECTOR).getAttribute("alt"))
                                            webView.engine.loadWorker.stateProperty().removeListener(this)
                                        }
                                    }
                                }
                            })
                            webView.engine.executeScript("selectCalendarDate(2017,4,28)")
                        }
                    }
                }
            }
        }

        scriptButton.setOnAction {
            webView.engine.executeScript(scriptField.text)
        }

    }


    fun WebEngine.loadAndThen(url: String, block: (Worker.State, Worker.State) -> Unit) {
        loadWorker.stateProperty().addListener(object : ChangeListener<Worker.State> {
            override fun changed(observable: ObservableValue<out Worker.State>, oldValue: Worker.State, newValue: Worker.State) {

                when (newValue) {
                    Worker.State.RUNNING -> Unit
                    Worker.State.READY -> Unit
                    Worker.State.SCHEDULED -> Unit
                    Worker.State.CANCELLED -> loadWorker.stateProperty().removeListener(this)
                    Worker.State.FAILED -> loadWorker.stateProperty().removeListener(this)
                    Worker.State.SUCCEEDED -> {
                        block(oldValue, newValue)
                        loadWorker.stateProperty().removeListener(this)
                    }
                }
            }
        })

        //val a = { a: Int -> a * a }

        //webView.engine::load.compose(webView.engine::wait)
        webView.engine.load(url)
    }

    fun <V, T, R> ((T) -> R).compose(before: (V) -> T) = { v: V -> this(before(v)) }
    fun <V, T, R> ((V) -> T).andThen(after: (T) -> R) = { v: V -> after(this(v)) }


    fun WebEngine.wait(block: () -> Unit) = {
        loadWorker.stateProperty().addListener(object : ChangeListener<Worker.State> {
            override fun changed(observable: ObservableValue<out Worker.State>, oldValue: Worker.State, newValue: Worker.State) {

                when (newValue) {
                    Worker.State.RUNNING -> Unit
                    Worker.State.READY -> Unit
                    Worker.State.SCHEDULED -> Unit
                    Worker.State.CANCELLED -> loadWorker.stateProperty().removeListener(this)
                    Worker.State.FAILED -> loadWorker.stateProperty().removeListener(this)
                    Worker.State.SUCCEEDED -> {
                        block()
                        loadWorker.stateProperty().removeListener(this)
                    }
                }
            }
        })
    }

    fun WebEngine.clickElementBySelectorAndThen(selector: String, block: (Worker.State, Worker.State) -> Unit) {
        loadWorker.stateProperty().addListener(object : ChangeListener<Worker.State> {
            override fun changed(observable: ObservableValue<out Worker.State>, oldValue: Worker.State, newValue: Worker.State) {
                when (newValue) {
                    Worker.State.RUNNING -> Unit
                    Worker.State.READY -> Unit
                    Worker.State.SCHEDULED -> Unit
                    Worker.State.CANCELLED -> loadWorker.stateProperty().removeListener(this)
                    Worker.State.FAILED -> loadWorker.stateProperty().removeListener(this)
                    Worker.State.SUCCEEDED -> {
                        block(oldValue, newValue)
                        loadWorker.stateProperty().removeListener(this)
                    }
                }
            }
        })
        executeScript("document.querySelector('$selector').click()")
    }

    fun WebEngine.inputTextToElementBySelectorAndThen(selector: String, text: String) {
        executeScript("document.querySelector('$selector').value='$text'")
    }

    fun WebEngine.getElementBySelector(selector: String)
            = executeScript("document.querySelector('$selector')") as HTMLElement

}