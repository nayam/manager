package jp.gr.java_conf.ny.index

object RsvNameSearchResult {
    val URL = "https://www.cm2.epss.jp/sendai/web/view/user/rsvNameSearchResult.html"
    val DO_SELECT_SELECTOR = "#doSelect"
}