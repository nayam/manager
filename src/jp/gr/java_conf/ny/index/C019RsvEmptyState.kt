package jp.gr.java_conf.ny.index

import java.time.DayOfWeek
import java.time.LocalDate
import java.time.temporal.TemporalAdjusters

object C019RsvEmptyState{
    val URL = "https://www.cm2.epss.jp/sendai/web/view/user/c019RsvEmptyState.html"
    val YEAR_SELECTOR = "#year--"
    val MONTH_SELECTOR = "#month--"
    val DAY_SELECTOR = "#day--"

    val MORNING_SELECTOR = "#isNotEmptyPager > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(1) > div > div > img"
    val AFTERNOON_SELECTOR = "#isNotEmptyPager > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(2) > div > div > img"
    val NIGHT_SELECTOR = "#isNotEmptyPager > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(3) > div > div > img"

    val PREVIOUS_MONTH_SELECTOR = "#calendar > table:nth-child(1) > tbody > tr > td > div > a:nth-child(1)"
    val NEXT_MONTH_SELECTOR = "#calendar > table:nth-child(1) > tbody > tr > td > div > a:nth-child(2)"
    val NEXT_MONTH_IF_THIS_MONTH_SELECTOR = "#calendar > table:nth-child(1) > tbody > tr > td > div > a"

    val YEAR_MONTH_ON_CALENDAR_SELECTOR = """#calendar > table:nth-child(1) > tbody > tr > td > div"""

    fun toSelector(date: LocalDate):String {
        val firstDay = date.with(TemporalAdjusters.firstDayOfMonth())

        val dayOfWeek = when (date.dayOfWeek) {
            DayOfWeek.SUNDAY -> 1
            DayOfWeek.MONDAY -> 2
            DayOfWeek.TUESDAY -> 3
            DayOfWeek.WEDNESDAY -> 4
            DayOfWeek.THURSDAY -> 5
            DayOfWeek.FRIDAY -> 6
            DayOfWeek.SATURDAY -> 7
            else -> error("Unknown Day of Week")
        }

        val skip = when (firstDay.dayOfWeek) {
            DayOfWeek.SUNDAY -> 0
            DayOfWeek.MONDAY -> 1
            DayOfWeek.TUESDAY -> 2
            DayOfWeek.WEDNESDAY -> 3
            DayOfWeek.THURSDAY -> 4
            DayOfWeek.FRIDAY -> 5
            DayOfWeek.SATURDAY -> 6
            else -> error("Unknown Day of Week")
        }
        val weekOfMonth = (date.dayOfMonth + skip - 1) / 7 + 2

        return "#calendar > table:nth-child(2) > tbody > tr:nth-child($weekOfMonth) > td:nth-child($dayOfWeek) > div > a"
    }
}