package jp.gr.java_conf.ny.index

object RsvNameSearch {
    val URL = "https://www.cm2.epss.jp/sendai/web/view/user/rsvNameSearch.html"
    val TEXT_KEYWORD_SELECTOR = "#textKeyword"
    val DO_SEARCH_SELECTOR = "#doSearch"
}
