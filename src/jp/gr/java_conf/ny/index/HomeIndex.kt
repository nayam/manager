package jp.gr.java_conf.ny.index

object HomeIndex {
    val URL by lazy { "https://www.cm2.epss.jp/sendai/web/view/user/homeIndex.html" }
    val RSV_NAME_SEARCH_SELECTOR by lazy { "#childForm > table > tbody > tr > td:nth-child(2) > table:nth-child(3) > tbody > tr > td:nth-child(1) > table > tbody > tr:nth-child(2) > td:nth-child(1) > a > img" }

}